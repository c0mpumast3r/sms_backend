from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager

import uuid


#AUTHENTICATION HANDLING
class CustomAccountManager(BaseUserManager):

    def create_superuser(self, phone, password, **other_fields):

        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)
        other_fields.setdefault('is_active', True)

        if other_fields.get('is_staff') is not True:
            raise ValueError(
                'Superuser must be assigned to is_staff=True.')
        if other_fields.get('is_superuser') is not True:
            raise ValueError(
                'Superuser must be assigned to is_superuser=True.')

        return self.create_user(phone, password, **other_fields)

    def create_user(self, phone, password, **other_fields):

        if not phone:
            raise ValueError(_('You must provide an phone'))

        user = self.model(phone=phone, **other_fields)
        user.set_password(password)
        user.save()
        return user


#USER
class User(AbstractBaseUser, PermissionsMixin):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=True)
    name = models.CharField(max_length=100)
    company_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    phone = models.CharField(max_length=100, unique=True)
    reason = models.CharField(max_length=100)
    credits = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    role_id = models.CharField(max_length=100)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)


    objects = CustomAccountManager()

    USERNAME_FIELD = 'phone'

    def __str__(self):
        return self.phone


#CONTACTS
class Contacts(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=True)
    contact_name = models.CharField(max_length=100)
    contact_number = models.CharField(max_length=100)
    contact_group = models.CharField(max_length=100)
    contact_id = models.CharField(max_length=100)
    
    def __str__(self):
        return self.contact_name


#MESSAGE
class Message(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=True)
    recipient = models.CharField(max_length=100)
    cost = models.CharField(max_length=100)
    message = models.CharField(max_length=100)
    sender = models.CharField(max_length=100)
    length = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    repetition = models.CharField(max_length=100)
    start_date = models.CharField(max_length=100)
    end_date = models.CharField(max_length=100)
    time = models.CharField(max_length=100)
    sel_day = models.CharField(max_length=100)
    message_id = models.CharField(max_length=100)
    
    def __str__(self):
        return self.recipient


#SENDER NAME
class SenderName(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=True)
    name = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    transaction_id = models.CharField(max_length=100)
    method = models.CharField(max_length=100)
    cost = models.CharField(max_length=100)
    paid = models.CharField(max_length=100)
    company_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    phone_no = models.CharField(max_length=100)
    renewal = models.CharField(max_length=100)
    validity = models.CharField(max_length=100)
    created = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name
     

#TRANSACTIONS
class Transactions(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=True)
    user_id = models.CharField(max_length=100)
    account = models.CharField(max_length=100)
    user = models.CharField(max_length=100)
    detail = models.CharField(max_length=100)
    credits = models.CharField(max_length=100)
    amount = models.CharField(max_length=100)
    vat = models.CharField(max_length=100)
    method = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    date = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name

     
