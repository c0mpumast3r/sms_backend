from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status, generics, mixins, viewsets
from rest_framework.authentication import SessionAuthentication, TokenAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import get_object_or_404
from rest_framework_simplejwt.authentication import JWTAuthentication

from django_filters.rest_framework import DjangoFilterBackend

#serializers
from .serializers import *
#models
from .models import *

# Create your views here.
#authentication_classes = [TokenAuthentication]

#USER
class User(viewsets.GenericViewSet, mixins.ListModelMixin,
                     mixins.UpdateModelMixin, mixins.RetrieveModelMixin,
                     mixins.DestroyModelMixin):

    serializer_class = UserSerializer
    queryset = User.objects.all()

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

class UserRegister(APIView):

    #permission_classes = [IsAuthenticated]

    #serializer_class = EntityPersonRegisterSerializer
    
    def post(self, request, *args, **kwargs):
        serializer = UserRegisterSerializer(data=request.data)
        if serializer.is_valid():
            newuser = serializer.save()
            if newuser:
                return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#CONTACTS
class Contacts(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin,
                     mixins.UpdateModelMixin, mixins.RetrieveModelMixin,
                     mixins.DestroyModelMixin):

    serializer_class = ContactsSerializer
    queryset = Contacts.objects.all()

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]


#MESSAGE
class Message(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin,
                     mixins.UpdateModelMixin, mixins.RetrieveModelMixin,
                     mixins.DestroyModelMixin):

    serializer_class = MessageSerializer
    queryset = Message.objects.all()

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]


#SENDER NAME
class SenderName(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin,
                     mixins.UpdateModelMixin, mixins.RetrieveModelMixin,
                     mixins.DestroyModelMixin):

    serializer_class = SenderNameSerializer
    queryset = SenderName.objects.all()

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]


#TRANSACTION
class Transactions(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin,
                     mixins.UpdateModelMixin, mixins.RetrieveModelMixin,
                     mixins.DestroyModelMixin):

    serializer_class = TransactionsSerializer
    queryset = Transactions.objects.all()

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

