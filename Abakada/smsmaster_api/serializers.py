from rest_framework import serializers

from .models import *

#USER
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('name', 'company_name', 'email' , 'phone' , 'reason', 'credits', 'status')

class UserRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('phone', 'name')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        # as long as the fields are the same, we can just use this
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

#CONTACTS
class ContactsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contacts
        fields = '__all__'
        
#MESSAGE
class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

#SENDER NAME
class SenderNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = SenderName
        fields = '__all__'

#TRANSACTION   
class TransactionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transactions
        fields = '__all__'
