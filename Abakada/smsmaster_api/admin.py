from django.contrib import admin
from .models import *

# Register your models here.

#MAIN
admin.site.register(User)
admin.site.register(Contacts)
admin.site.register(Message)
admin.site.register(SenderName)
admin.site.register(Transactions)