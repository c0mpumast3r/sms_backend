from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import *
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

api_router = DefaultRouter()


#USER
api_router.register('User', User, basename='User')

#CONTACTS
api_router.register('Contacts', Contacts, basename='Contacts')

#MESSAGE
api_router.register('Message', Message, basename='Message')

#SENDER NAME
api_router.register('SenderName', SenderName, basename='SenderName')

#TRANSACTION
api_router.register('Transactions', Transactions, basename='Transactions')

#URL PATTERNS
urlpatterns = [

    path('api/', include(api_router.urls)),
    
    #TOKENS
    path('api/UserRegister/', UserRegister.as_view(), name='create_user'),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/rest-auth/', include('rest_auth.urls')),
]