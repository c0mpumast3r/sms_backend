from django.apps import AppConfig


class SMSMasterApiConfig(AppConfig):
    name = 'smsmaster_api'
